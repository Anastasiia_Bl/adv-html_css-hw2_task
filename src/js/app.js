// импортировать все функции с functions.js
import * as flsFunctions from "./modules/functions.js";
flsFunctions.isWebp();


// Бургер меню
let burgerLogo = document.querySelector('.header__burger');
burgerLogo.addEventListener('click', ()=> {
    document.querySelector('.header__burger').classList.toggle('active');
    document.querySelector('.header__menu-list').classList.toggle('active');
})
// Закриття меню при кліку не в його межах
document.addEventListener('click', (e)=> {
    let click = e.composedPath().includes(burgerLogo);
    if (!click) {
        document.querySelector('.header__burger').classList.remove('active');
        document.querySelector('.header__menu-list').classList.remove('active');
    }
})
